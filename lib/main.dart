import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(appBar: AppBar(), body: HomeWidget()),
    );
  }
}

class HomeWidget extends StatelessWidget {
  Function _onPressed() {
    return () => print("Cliquei aqui");
  }

  ListTile _buildListTile() {
    return ListTile(
      leading: Icon(Icons.android),
      title: Text("Item 01"),
      subtitle: Text("Subtitle"),
      onTap: _onPressed(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile(),
          _buildListTile()
        ],
      ),
    );
  }
}
